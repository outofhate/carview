package com.outofdate.carview

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.PropertyValuesHolder.ofFloat
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import kotlin.math.absoluteValue


class TankView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private val carHeight = context.resources.getDimensionPixelSize(R.dimen.g_4)
    private val carWidth = context.resources.getDimensionPixelSize(R.dimen.g_2)
    private var car =
        RectF(-(carWidth / 2f), -(carHeight / 2f), carWidth / 2f, carHeight / 2f)
    private val position = PointF(0f, 0f)
    private var angle = 0f
    private var lastAngle = 0f
    private var fixedAngle = 0f
    private val paint = Paint()
    private val moveAnimator = ValueAnimator()
    private val rotateAnimator = ValueAnimator()

    init {
        paint.style = Paint.Style.FILL_AND_STROKE
        paint.strokeWidth = carWidth.toFloat() / 4
        moveAnimator.duration = 500
        moveAnimator.addUpdateListener {
            val x = it.getAnimatedValue("x") as Float
            val y = it.getAnimatedValue("y") as Float
            car.set(x, y, x + carWidth, y + carHeight)
            invalidate()
        }
        moveAnimator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {

            }
        })
        rotateAnimator.duration = 500
        rotateAnimator.addUpdateListener {
            angle = it.animatedValue as Float
            invalidate()
        }
        rotateAnimator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                angle = fixedAngle
                lastAngle = angle
                moveAnimator.setValues(
                    ofFloat("x", car.centerX() - (carWidth / 2), position.x - (carWidth / 2)),
                    ofFloat("y", car.centerY() - (carHeight / 2), position.y - (carHeight / 2))
                )
                moveAnimator.start()
            }
        })
        isSaveEnabled = true
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (rotateAnimator.isRunning || moveAnimator.isRunning) {
            return true
        }
        position.set(event.x, event.y)
        getRotationToPoint()
        rotateAnimator.setFloatValues(lastAngle, angle)
        rotateAnimator.start()
        return true
    }

    override fun onDraw(canvas: Canvas) {
        paint.color = Color.YELLOW
        paint.style = Paint.Style.FILL
        canvas.save()
        canvas.rotate(angle, car.centerX(), car.centerY())
        canvas.drawRect(car, paint)
        paint.color = Color.BLACK
        paint.style = Paint.Style.STROKE
        canvas.drawLine(car.left, car.bottom, car.right, car.bottom, paint)
        canvas.restore()
    }

    private fun getRotationToPoint() {
        val x = position.x - car.centerX()
        val y = position.y - car.centerY()
        val degree = (Math.atan2(x.toDouble(), y.toDouble()))
        angle = -Math.toDegrees(degree).toFloat()
        fixedAngle = angle
        if (lastAngle - 180 > -180 && lastAngle > 0 && angle < -90) {
            val fix = 180 - angle.absoluteValue
            angle = 180 + fix
        } else if (lastAngle + 180 > 0 && lastAngle < 0 && angle > 90) {
            angle -= 360
        }
    }

    override fun onSaveInstanceState(): Parcelable? {
        val superState = super.onSaveInstanceState()
        val savedState = SavedState(superState)
        savedState.car = car
        savedState.angle = angle
        return savedState
    }

    public override fun onRestoreInstanceState(state: Parcelable) {
        if (state !is SavedState) {
            super.onRestoreInstanceState(state)
            return
        }
        angle = state.angle
        lastAngle = angle
        car = state.car!!
        super.onRestoreInstanceState(state.superState)

    }

    class SavedState : BaseSavedState {

        internal var car: RectF? = null
        internal var angle: Float = 0f


        constructor(`in`: Parcel) : super(`in`) {
            this.car = `in`.readParcelable(Rect::class.java.classLoader)
            this.angle = `in`.readFloat()
        }

        constructor(superState: Parcelable) : super(superState) {}

        override fun writeToParcel(dest: Parcel, flags: Int) {
            super.writeToParcel(dest, flags)
            dest.writeParcelable(this.car, flags)
            dest.writeFloat(this.angle)
        }

        companion object {
            @JvmField
            val CREATOR: Parcelable.Creator<SavedState> = object : Parcelable.ClassLoaderCreator<SavedState> {
                override fun createFromParcel(source: Parcel, loader: ClassLoader): SavedState {
                    return SavedState(source)
                }

                override fun createFromParcel(`in`: Parcel): SavedState {
                    return SavedState(`in`)
                }


                override fun newArray(size: Int): Array<SavedState?> {
                    return arrayOfNulls(size)
                }
            }
        }
    }
}